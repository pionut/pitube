<?php
	require __DIR__ . '/Pitube.php';
	$pitube = new pitube;
	
	if($_GET['url']) {
		if(file_exists(__DIR__ .'/downloads/'.$pitube->extract_youtube_id(strip_tags(trim($_GET['url']))).".mp3")) {
			$song_info = $pitube->song_info(strip_tags(trim($_GET['url'])));
			$download['file']['id'] = $pitube->extract_youtube_id(strip_tags(trim($_GET['url'])));
			$download['file']['title'] = $song_info["title"];
		} else {
			$download = $pitube->download(strip_tags(trim($_GET['url'])));
		}
		
		if($download) {
			$array = array(
				'id'		=> $download['file']['id'],
				'url'		=> 'https://www.pitube.ml/downloads/'.$download['file']['id'].'.mp3',
				'title' 	=> $download['file']['title'],
				'size'		=> $download['file']['size'],
				'thumb'		=> $download['file']['thumb'],
				'added_on'	=> date("d/m/Y", filemtime($download['file']['location']))
			);
			
			echo json_encode(array("songinfo"=> $array));
		}
	}
	
	if($_GET['check_for_new_files']) {
		$files = array();
		foreach ($pitube->generate_new_files_list() as $row ) {
			$files[$row['id']] = "https://www.youtube.com/watch?v=".$row['youtube_id'];
		}
		$pitube->update_downloads_android();
		echo json_encode($files);
	}
?>