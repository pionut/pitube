var youtube_api_key = "AIzaSyA-v28BXnmmkRdyf6beGhRmkjs74AM-Gks";
var vid = document.getElementById("audio_player");
const playing = new Object();

$(window).scroll(function() {
	if ($(this).scrollTop() > 850) {
		$("#onpage-nav").addClass('page-nav-scrolled');
		$("#showcase").addClass('no-animation');
	} else {
		$("#onpage-nav").removeClass('page-nav-scrolled');
		$("#showcase").removeClass('no-animation');
	}
});
(function($) {
	$("#ylink").focus();
	vid.autoplay	= false;
	vid.controls	= false;
	vid.preload	= "none";

	$('.nav-toggle').click(function() {
        $('body').toggleClass('nav-open')
    });
	$(document).keypress(function (e) {
        if (e.which === 13) {
            $(".download").trigger("click");
        }
    });
	$(document).on("click", ".download", function() {
		youtube_validate($("#ylink").val());
	});
	$(document).on("click", ".removeme", function() {
		$(this).remove();
	});
	$(document).on("click", ".godownloads", function() {
		switch_menu("#main_menu","#download_menu","fast");
	});
	$(document).on("click", ".gomenu", function() {
		switch_menu("#download_menu","#main_menu","fast");
	});
	$( "#expand-download" ).click(function() {
		// $( "#expanded-content" ).slideToggle( "slow" );
		switch_menu("#download_menu","#main_menu","fast");
	});
	$( "#download_all_new_files" ).click(function() {
		$.each(new_files_list, function(key, value) {
			window.open("https://www.pitube.ml/downloads/?vid="+ value.youtube_id, '_blank');
		});
	});

	$(document).on("click", ".progress", function() {
		$("#player-play-pause").removeClass('fa-play');
		$("#player-play-pause").addClass('fa-pause');
		$(".player_song_title").html($("#title_"+$(this).parent().attr("id")).html());
		vid.src = "https://www.pitube.ml/downloads/"+$(this).parent().attr("id")+".mp3";
		vid.play();
	});

	//player
	$(document).on("click", "#player-play-pause", function() {
		if (vid.paused == false) {
			vid.pause();
			$("#player-play-pause").removeClass('fa-pause');
			$("#player-play-pause").addClass('fa-play');
		} else {
			$("#player-play-pause").removeClass('fa-play');
			$("#player-play-pause").addClass('fa-pause');
			vid.play();
		}
	});

	
	$(document).on("click", ".ready_to_play", function() {
		if(playing.button_id!=""){
			$("#"+playing.button_id+" i").removeClass('fa-pause');
			$("#"+playing.button_id+" i").addClass('fa-play');
		}
		playing.button_id	= $(this).parent().attr('id');
		playing.song		= $(this).parent().parent().attr('id');
		
		console.log(playing);
		if($(this).hasClass('fa-play')) {
			$(this).removeClass('fa-play');
			$(this).addClass('fa-pause');
		} else {
			$(this).addClass('fa-play');
			$(this).removeClass('fa-pause');
		}
	});
	
	// $(document).on("click", ".fa-step-forward", function() {
		// if(playing.song!=""){
			// var next_song = $("#"+playing.song).next().attr("id");
			// console.log("next song: ",next_song);
			// $("#"+next_song+" #".).addClass("dickkkkkkkkkkkkkkkkkkkkkkkkkkkkk");
		// }
	// });
	
	//end player
})(jQuery);

function switch_menu(from_div, to_div,speed) {
	$(from_div).hide(speed, function(){
		$(to_div).show(speed);
	});
	return true;
}

function download(video_id) {
	$.ajax({
		type: "GET",
		url: "test.php",
		data: {
			"identifier": "https://www.youtube.com/watch?v="+video_id
		},
		dataType: "json",
		xhrFields: {
			onprogress: function(e) {
				// $('#debug').html(e.target.responseText);
				// console.log(e.target.responseText);
				// $('#ylink').val("Downloading video.. Can take a while!");
			}
		},
		success: function(data) {
			$("#"+video_id).addClass("ready");
			$("#progress_"+video_id).html("<i class='fa fa-play' aria-hidden='true'></i>");
			$("#action_"+video_id).html("<div class='done'><a href='http://www.pitube.ml/downloads/?vid="+ data['id'] +"' title='Download'><i class='fa fa-download' aria-hidden='true'></i></a></div>");
			if($('input[name="autodownload"]').is(':checked'))
				{
					redirect("https://www.pitube.ml/downloads/?vid="+ data['id']);
				}
			return data;
		},
		error: function(result) {
			$("#"+video_id).addClass("ready removeme");
			$("#progress_"+video_id).html("<i class='fa fa-times' aria-hidden='true'></i>");
			$("#action_"+video_id).html("<div class='error'><a title='"+result.responseJSON.message+"'>Error</a></div>");
			return result;
		},
		complete: function(result) {
			return result;
		}
	});
}
function playlist(id) {
	var playlist_id = extract_playlist_id(id);
	var youtube_api_url = "https://www.googleapis.com/youtube/v3/playlistItems?playlistId="+playlist_id+"&maxResults=50&part=snippet&key="+youtube_api_key;
	$.getJSON(youtube_api_url,function(data,status,xhr){
		var obj = data['items'];
		for(var i = 0; i < obj.length; i++) {
		  var video = obj[i];
		  get_video("https://www.youtube.com/watch?v="+video['snippet']['resourceId']['videoId']);
		}
	});
}

function youtube_validate(url) {
	if($('input[name="disable_playlist"]').is(':checked'))
	{
		get_video(url);
	} else {
		var playlist_identifier = "&list=";
		if(url.indexOf(playlist_identifier) >= 0){
			ConfirmDialog(url,'Download all songs from this playlist? \n Press "Cancel" if you want to download only that song, not the playlist! :) \n Press "Ok" if you want to download all songs from the playlist!');
		} else {
			get_video(url);
		}
	}

	switch_menu("#main_menu","#download_menu","slow");
	$("#ylink").val('');
}

function get_video(id) {
	youtube_video_id = extract_video_id(id);
	var youtube_api_url = "https://www.googleapis.com/youtube/v3/videos/?id="+youtube_video_id+"&part=snippet&key=" + youtube_api_key;
	$.getJSON(youtube_api_url,function(data,status,xhr){
		if($.isPlainObject(data['items'][0])) {
			$("#music-player").after('<div id="'+youtube_video_id+'" thumb="'+data['items'][0]['snippet']['thumbnails']['high']['url']+'" class="item"><div class="progress" id="progress_'+youtube_video_id+'"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></div><div class="title" id="title_'+youtube_video_id+'">'+data['items'][0]['snippet']['title']+'</div><div class="action" id="action_'+youtube_video_id+'">Downloading</div></div>');
			download(data['items'][0]['id']);
		} else {
			// search a video on youtube
			var search_song = "https://www.googleapis.com/youtube/v3/search?q="+id+"(Official Vídeo)&maxResults=3&part=snippet&type=video&key=" + youtube_api_key;
			$.getJSON(search_song,function(result_list,status,xhr){
				console.log(result_list['items']);
				$("#search_results").show("fast");
				$.each(result_list['items'], function( index, value ) {
					//$("#search_results").before('<div class="item ready">'+index+': '+value['snippet']['title']+'</div>');
					$("#search_results br").after('<label for="modal-trigger"><div id="'+value['id']['videoId']+'" thumb="'+value['snippet']['thumbnails']['medium']['url']+'" title="'+value['snippet']['title']+'" class="item search-item">'+value['snippet']['title']+'</div></label>');
				});
				//old error
				//$("#music-player").after('<div id="'+youtube_video_id+'" class="item ready removeme">Looks like <b>this URL is not valid</b> ;(</div>');
			});
		}
	});
}

	function extract_playlist_id(url){
		var reg = new RegExp("[&?]list=([a-z0-9_]+)","i");
		var match = reg.exec(url);

		if (match&&match[1].length>0&&extract_video_id(url)){
			return match[1];
		}else{
			return "error";
		}
	}

	function extract_video_id(url){
		var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
		var match = url.match(regExp);
		return (match&&match[7].length==11)? match[7] : "error";
	}

	function ConfirmDialog(user_input,message) {
		if(confirm(message)){
			playlist(user_input);
		}
		else{
			get_video(user_input);
		}
	};

	function update_url(url, filename) {
		ga('set', {
			page: url,
			title: filename
		});
		ga('send', 'pageview');
		history.pushState(null, filename, url);
	}

	function redirect (url) {
		var ua        = navigator.userAgent.toLowerCase(),
			isIE      = ua.indexOf('msie') !== -1,
			version   = parseInt(ua.substr(4, 2), 10);
		// Internet Explorer 8 and lower
		if (isIE && version < 9) {
			var link = document.createElement('a');
			link.href = url;
			document.body.appendChild(link);
			link.click();
		}
		// All other browsers can use the standard window.location.href (they don't lose HTTP_REFERER like Internet Explorer 8 & lower does)
		else {
			if($('input[name="alternative_download"]').is(':checked')) {
				window.open(url, '_blank');
			} else {
				window.location.href = url; 
			}
		}
	}
	if($('input[name="background_on_mouseover"]').is(':checked')) {
		$(document).on("click", ".stored .item", function() {
			$("#player-play-pause").removeClass('fa-play');
			$("#player-play-pause").addClass('fa-pause');
			$(".player_song_title").html($("#title_"+$(this).attr("id")).html());
			vid.src = "https://www.pitube.ml/downloads/"+$(this).attr("id")+".mp3";
			vid.play();
		});
		$(document).on("mouseenter", ".item", function() {
			$('.box').css('background-image', 'url(' + $(this).attr("thumb") + ')');
			
		});
		$(document).on("mouseleave", ".item", function() {
			$('.box').css('background-image', '');
		});
	}
	$(document).on("click", ".search-item", function() {
		$("#modal-title").html($(this).attr("title"));
		$("#modal-src").attr("src","https://www.youtube.com/embed/"+$(this).attr("id")+"?autoplay=1");
		$("#modal-download").html("<a href='/watch?v="+$(this).attr("id")+"'>Download</a>");
	});