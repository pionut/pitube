<?php
// Load the stamp and the photo to apply the watermark to
if(isset($_GET['file'])) {
	$request = filter_input(INPUT_GET, 'file', FILTER_SANITIZE_SPECIAL_CHARS);
	$stamp0 = imagecreatefrompng('play.png');
	$stamp1 = imagecreatefrompng('download.png');
	$stamp2 = imagecreatefrompng('logo.png');
	// $im = imagecreatefromjpeg($request);
	$im = imagecreatefromjpeg("https://i.ytimg.com/vi/".$request."/hqdefault.jpg");

	$imgx = imagesx($im);
	$imgy = imagesy($im);
	
	$center0X=10;
	$center0Y=10;

	$center1X=10;
	$center1Y=100;
	
	$center2X=10;
	$center2Y= 275;

	// Copy the stamp image onto our photo using the margin offsets and the photo 
	// width to calculate positioning of the stamp. 
	imagecopy($im, $stamp0, $center0X, $center0Y, 0, 0, imagesx($stamp0), imagesy($stamp0));
	imagecopy($im, $stamp1, $center1X, $center1Y, 0, 0, imagesx($stamp1), imagesy($stamp1));
	imagecopy($im, $stamp2, $center2X, $center2Y, 0, 0, imagesx($stamp2), imagesy($stamp2));

	// Output and free memory
	header('Content-type: image/png');
	imagepng($im);
	imagedestroy($im);
}