<?php

$file_id = stripslashes($_GET['vid']);

if(!isset($file_id)) { exit; }

require '../Pitube.php';
$pt = new pitube;


$local_file = $file_id.'.mp3';
$download_file = $pt->get_title("https://www.youtube.com/watch?v=".$file_id) .'.mp3';
$new_title = str_replace(","," ",$download_file);
// set the download rate limit (=> 20,5 kb/s)
$download_rate = 1200.0;
if(file_exists($local_file) && is_file($local_file))
{
    header('Cache-control: private');
    header('Content-Type: application/octet-stream');
    header('Content-Length: '.filesize($local_file));
    header('Content-Disposition: filename='.$new_title);

    flush();
    $file = fopen($local_file, "r");
    while(!feof($file))
    {
        // send the current file part to the browser
        print fread($file, round($download_rate * 1024));
        // flush the content to the browser
        flush();
        // sleep one second
        sleep(1);
    }
    fclose($file);
	$pt->update_downloads($file_id);
	}
else {
    die();
}

?>