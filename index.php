<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

require __DIR__ . '/Pitube.php';
$pt = new pitube;

function t($string, $replace = null) {
	$array = array(
			"GB"	=> array(
				"home"				=> "Home",
				"facebook"			=> "Facebook",
				"download"			=> "Download",
				"downloads"			=> "Downloads",
				"done"				=> "<i class='fa fa-download' aria-hidden='true'></i>",
				"share"				=> "<i class='fa fa-facebook' aria-hidden='true'></i>",
				"hello"				=> "Hello",
				"more"				=> "More",
				"main_menu"			=> "Settings",
				"donate"			=> "Support this project",
				"welcome_back"		=> "welcome back",
				"coose_os_text"		=> "iOS? Windows? Android? Linux?<br>Witch one?",
				"enable_auto_dld"	=> "Enable automatic download",
				"dis_playlist_dld"	=> "Disable playlist download",
				"workinprogress"	=> "<h5>We are working on some improvements for the next couple of weeks!</h5><h5>Please Subscribe to notifications and we will let you know when everything is ready !</h5>",
				"small_text_android"=> "(This application it's in BETA stage)",
				"input_placeholder"	=> "What do you wanna listen? :)",
			),
			"RO"	=> array(
				"home"				=> "Acasa",
				"facebook"			=> "Facebook",
				"download"			=> "Descarca",
				"downloads"			=> "Descarcari",
				"done"				=> "Gata",
				"share"				=> "<i class='fa fa-facebook' aria-hidden='true'></i>",
				"hello"				=> "Salut",
				"more"				=> "Extra",
				"main_menu"			=> "Setari",
				"donate"			=> "Doneaza",
				"welcome_back"		=> "bun venit inapoi",
				"coose_os_text"		=> "iOS? Windows? Android? Linux?<br>Care din ele?",
				"enable_auto_dld"	=> "Descarca automat dupa convertire",
				"dis_playlist_dld"	=> "Dezactiveaza descarcare playlist",
				"workinprogress"	=> "<h5>Acest proiect este in lucru!</h5><h5>Imi pare rau pentru eventualele probleme!</h5>",
				"small_text_android"=> "(Aceasta aplicatie este BETA)",
				"input_placeholder"	=> "Ce vrei sa asculti? :)",
			)
	);
	$client_country = $GLOBALS['pt']->get_country($_SERVER['REMOTE_ADDR']);
	if (array_key_exists($client_country, $array)) {
		return $array[$client_country][$string];
	} else {
		return $array['GB'][$string];
	}
	// return $array['BG'][$string];
}

if(isset($_GET['v'])) {
	$v_link		= "https://www.youtube.com/watch?v=".filter_input(INPUT_GET, 'v', FILTER_SANITIZE_SPECIAL_CHARS);
	$v_name		= htmlspecialchars($pt->get_title($v_link));
	$v_thumb	= "https://www.pitube.ml/thumb.php?v=".filter_input(INPUT_GET, 'v', FILTER_SANITIZE_SPECIAL_CHARS);
	//$v_thumb	= $pt->song_info($v_link)['thumbnail_url'];
	// $v_thumb	= "https://www.pitube.ml/pitemplate/ogimage.php?file=".filter_input(INPUT_GET, 'v', FILTER_SANITIZE_SPECIAL_CHARS);
}
?>
<!DOCTYPE html>
<html lang="en" class="tr-directwrite tr-aa-unknown-subpixel">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Youtube to mp3 converter and downloader - PiTube - Android</title>
		<meta property="og:description" name="description" content="PiTube is an easy to use, yet powerful, opensource YouTube to mp3 converter for Windows / Android / MAC / Linux.">
		<meta property="og:image" content="<?=(isset($_GET['v']) ? $v_thumb : 'https://www.pitube.ml/pitemplate/ogimage.png')?>" />
		<meta property="og:type" content="website" />
		<meta property="og:locale" content="en_GB" />
		<meta property="og:locale:alternate" content="ro_RO" />
		<meta property="og:locale:alternate" content="en_GB" />
		<meta property="og:site_name" content="PiTube" />
		<meta property="og:title" content="<?=(isset($_GET['v']) ? $v_name.' - '.t("download").' HQ MP3 using' : '')?> PiTube" />
		<meta property="og:url" content="https://www.pitube.ml<?=$_SERVER['REQUEST_URI']?>" />
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
			 (adsbygoogle = window.adsbygoogle || []).push({
				  google_ad_client: "ca-pub-6293250485535997",
				  enable_page_level_ads: true
			 });
		</script>
		<link rel="icon" type="image/png" href="https://www.pitube.ml/pitemplate/ogimage.png">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="/pitemplate/style.css" type="text/css" rel="stylesheet">
		<link href="/pitemplate/modal.css" type="text/css" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" type="text/javascript"></script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106314635-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());
		  gtag('config', 'UA-106314635-1');
		</script>
	</head>
	<body>
		<div id="overlay-wrapper">
			<!---
			Disable Navigation Button
			<div class="nav-toggle">
				<div class="icon"></div>
			</div>
			-->
			<div class="overlay-nav">
				<nav id="menu">
					<ul class="navigation">
						<li class="active"><a href="/"><span><?=t("home")?></span></a></li>
						<li class=""><a href="https://www.facebook.com/PiTube-Free-YouTube-to-MP3-154280518534284/" target="_blank"><span>Facebook</span></a></li>
					</ul>
				</nav>
			</div>
		</div>
		<div id="onpage-nav" class="sb-slide">
			<div id="color-divider"></div>
			<div class="container">
				<ul id="page-nav" class="navigation">
					<li class="active"><a href="/"><span><?=t("home")?></span></a></li>
					<li class=""><a href="https://www.facebook.com/PiTube-Free-YouTube-to-MP3-154280518534284/" target="_blank"><span>Facebook</span></a></li>
				</ul>
			</div>
		</div>
		<div id="sb-site">
			<section id="showcase" class="stars-active">
				<div class="stars" id="stars"></div>
				<div class="stars" id="stars2"></div>
				<div class="stars" id="stars3"></div>
				<div class="stars" id="stars4"></div>
				<div id="stars-overlay"></div>
				<div id="blue-planet-wrapper">
					<img id="blue-planet" src="./pitemplate/blue-planet.svg">
				</div>
				<img id="moon" src="./pitemplate/moon.svg">
				<img id="gas-planet" src="./pitemplate/gas-planet.svg">
				<div id="blue-planet-overlay"></div>
				<div id="down-arrow">
					<a href="https://www.pitube.ml/"><i class="fa fa-angle-down pulse infinite animated"></i></a>
				</div>
				<header class="hide-mobile">
					<div class="container">
						<div class="row">
							<a id="logo" href="https://www.pitube.ml/">

							</a>
							<nav id="menu">
								<ul class="navigation">
									<li class="active"><a href="/"><span><?=t("home")?></span></a></li>
									<li class=""><a href="https://www.facebook.com/PiTube-Free-YouTube-to-MP3-154280518534284/" target="_blank"><span>Facebook</span></a></li>
								</ul>
							</nav>
							<div class="social-wrapper">
								<ul class="quick-links social-buttons">
									<li><a href="https://www.facebook.com/PiTube-Free-YouTube-to-MP3-154280518534284/"><i class="fa fa-fw fa-facebook "></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</header>
				<div class="container">
					<div class="row" id="showcase-content">
						<div class="content hide-mobile">
							<div class="content revealOnScroll animated fadeIn" data-animation="fadeIn" data-timeout="100">
								<h1>PI<strong>TUBE</strong></h1>
							</div>
						</div>
						<div class="container downloads revealOnScroll animated fadeIn" data-animation="fadeIn" data-timeout="500">
							<div id="submit_song_form">
								<div class="validate" novalidate="">

									<div>
										<div class="input-body">
											<input type="text" title="Search a song by it's name or paste a link" id="ylink" class="email" placeholder="<?=t("input_placeholder")?>" required="">
											<div class="clear"><button id="convert" class="button download"><?=t("download")?></button></div>
										</div>
										<br/>
										
										<div id="search_results" class="form-group box">
											<br />
										</div>
										<br/>
										
										<div class="form-group stored box">
												<div id="download_menu">
													<?php
														$new_files = 0;
														$list_item= "";
														foreach ($pt->get_history() as $row ) {
															if($row['downloads']==0) $new_files++;
															$list_item .= '<div id="'.$row['youtube_id'].'" thumb="'.$row['thumb'].'" class="item ready"><div class="progress" id="progress_'.$row['youtube_id'].'"><i class="ready_to_play fa fa-play" aria-hidden="true"></i></div><div class="title" id="title_'.$row['youtube_id'].'">'.$row['title'].'</div><div class="action" id="action_'.$row['youtube_id'].'"><div class="done"><a title="share on Facebook" href="http://api.addthis.com/oexchange/0.8/forward/facebook/offer?url=https://www.pitube.ml/watch?v='.$row['youtube_id'].'" target="_blank" rel="nofollow">'.t("share").'</a>  <a href="http://www.pitube.ml/downloads/?vid='.$row['youtube_id'].'" title="Download">'.t("done").'</a></div></div></div>';
														}
													?>
													<div id="music-player" class="downloads-info">
														<span class="player-control">
															<i class="fa fa-step-backward"></i>
															<i id="player-play-pause" class="fa fa-play"></i>
															<i class="fa fa-step-forward"></i>
															<span class="player_song_title"></span>
															<audio id="audio_player">
																Your browser does not support the audio element.
															</audio>
														</span>
														<a id="expand-download"><small><?=t("main_menu")?> </small><i class="fa fa-cog"></i></a>
														<br />
														<div id="expanded-content" style="display:none">
															<div>You have <b><?=$pt->count_user_files()?></b> file<small>(s)</small> 	<button id="download_all_files_again" title="Download all files again">Download All</button></div><br />
															<div><b><?=$new_files?></b> file<small>(s)</small> waiting to be downloaded!<button id="download_all_new_files" title="Download new files">Download now</button></div>
														</div>

													</div>
													<?=$list_item?>
													<div class="navigation_button gomenu">
														<?=t("main_menu")?> <i class="fa fa-cog"></i> 
													</div>
												</div>
												<div style="display:none;" id="main_menu">
													<div>
														<label><small><?=t("hello")?></small> <label for="modal-trigger"><?=$pt->get_user_info("real_name")?></label><small>, <?=t("welcome_back")?>!</small></label><br>
														<?php if($new_files>=1){?><label><small>You have <?=$new_files?> new files! Look for them in download menu.</small></label><br><?php } ?>
														<hr>
													</div>
													<div>
														<input type="checkbox" checked id="autodownload" name="autodownload">
														<label title="Download MP3 file when conversion it's done :)" for="autodownload"><?=t("enable_auto_dld")?>?</label>
													</div>
													<div>
														<input disabled type="checkbox" checked id="disable_playlist" name="disable_playlist">
														<label title="EXPERIMENTAL" for="disable_playlist"><?=t("dis_playlist_dld")?></label>
													</div>
													<div>
														<input type="checkbox" id="alternative_download" name="alternative_download">
														<label title="ATTENTION: Please allow popups if you use this method!" for="alternative_download">Use alternative download method</label>
													</div>
													<div>
														<input type="checkbox" checked id="background_on_mouseover" name="background_on_mouseover">
														<label title="Disable if you have a slow computer" for="background_on_mouseover">Image background on mouse over</label>
													</div>
													<!--<?=t('workinprogress')?>-->
													<div class="navigation_button godownloads"><i class="fa fa-arrow-left"></i> <?=t("downloads")?></div>
												</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<div id="body" class="section modular">
				<div id="download"></div>
				<!--<div id="modular-choose" class="modular-section ">
					<div class="container">
						<div class="padding bigger revealOnScroll animated fadeIn" data-animation="fadeIn" data-timeout="200">
							<h1><?=t("download")?></h1>
							<h4><?=t("coose_os_text")?></h4>
						</div>
						<div>
							<div class="col revealOnScroll animated fadeIn" data-animation="fadeIn" data-timeout="500">
								<div class="block blue">
									<div>
										<i class="fa fa-android fa-5x"></i>
										<span>Android<hr>
											<small><a href="/app-debug.apk" class="button button-outline button-fancy2" title="<?=t("download")?> PiTube v0.02 for Android"><?=t("download")?></a></small>
										</span>
									</div>
								</div>
							</div>
							<div class="col revealOnScroll animated fadeIn" data-animation="fadeIn" data-timeout="900">
								<div class="block green">
									<div>
										<i class="fa fa-windows fa-5x"></i>
										<span>Windows<hr><small>Only for browser</small></span>
									</div>
								</div>
								<p></p>
							</div>
							<div class="col revealOnScroll animated fadeIn" data-animation="fadeIn" data-timeout="1300">
								<div class="block purple">
									<div>
										<i class="fa fa-apple fa-5x"></i>
										<span>iOS<hr><small>Unavaiable</small></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div id="limitless"></div>
				<div id="modular-limitless" class="modular-section ">
					<div class="container revealOnScroll animated fadeIn" data-animation="fadeIn" data-timeout="200">
						<div class="padding more bigger">
							<h2>There are no limits!</h2>
							<p>---</p>
						</div>
						<a href="https://www.pitube.ml/downloads/skeletons" class="button button-outline button-fancy2">
						TEST
						</a>
						<div class="top-divider">
							<div class="block blue"></div>
							<div class="block green"></div>
							<div class="block purple"></div>
						</div>
					</div>
				</div>-->
			</div>
			<footer>
				<div class="container">
					<div class="padding">
						<p>
							<a title="PiTube for Android" href="/app-debug.apk"><i class="fa fa-android"></i> <small><?=t("download")?></small> <b>PiTube</b><small> for Android</small></a>
							</br><small><?=t("small_text_android")?></small>
						</p>
						<p><a title="PiTube Facebook page" href="https://www.facebook.com/PiTube-Free-YouTube-to-MP3-154280518534284/"><i class="fa fa-facebook"></i> Follow PiTube on Facebook</a></p>
						<p><a href="mailto:p.ionut196@gmail.com"><i class="fa fa-envelope"></i>  Contact</a></p>
						
						</br>
						<p>
							<a title="PiTube <?=t("home")?>" href="https://www.pitube.ml/">PiTube</a></br><i title="Coded" class="fa fa-code"></i> with <i title="love" class="fa fa-heart"></i>
						</p>
						</br>
						
						<p>No rights reserved</p>
						<p>No content hosted on this server</p>
						<p>We do not take any responsibility and we are not liable for any damage caused through use of products or services through this website, be it indirect, special, incidental or consequential damages (including but not limited to damages for loss of business, loss of profits, interruption or the like).</p>
					</div>
				</div>
			</footer>
		</div>
		<div class="modal">
		  <input id="modal-trigger" class="checkbox" type="checkbox">
		  <div class="modal-overlay">
			<label for="modal-trigger" class="o-close"></label>
			<div class="modal-wrap a-center">
			  <label for="modal-trigger" class="close">&#10006;</label><br><hr>
			  <iframe id="modal-src" frameborder="0" src=""></iframe>
			  <br><hr />
			  <h3 id="modal-title">..</h3>
			  <h3 id="modal-download">Download</h3>
			</div>
		  </div>
		</div>
		<div class="donation hide-mobile"><a href="https://paypal.me/pionut196" target="_blank"><i class="fa fa-paypal"></i> <?=t("donate")?></a></div>
	</body>
	<script async type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a5856e31bc0585e"></script>
	<script src="pitemplate/function.js"></script>
	<link rel="manifest" href="/manifest.json" />
	<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
	<script>
	  var OneSignal = window.OneSignal || [];
	  OneSignal.push(function() {
		OneSignal.init({
		  appId: "9bbd3dc2-308d-4e8c-8670-f69c110a3b9e",
		});
	  });

	<?php
	$list_item = '';
	foreach ($pt->generate_new_files_list() as $row ) {
		$list_item .= $row['id'].':{youtube_id:"'.$row['youtube_id'].'"},';
	}

	if(isset($_GET['v'])) {
		echo '$("input").val("'.$v_link.'");';
		echo '$(".download").trigger("click");';
	}
	?>
	var new_files_list = {<?=$list_item?>};
	</script>
</html>
