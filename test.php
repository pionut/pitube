<?php
	require __DIR__ . '/Pitube.php';
	$pitube = new pitube;
	if(isset($_GET['identifier'])) {
	$download = $pitube->download(strip_tags(trim($_GET['identifier'])));
	if($download) {
		$array = array(
			'id'		=> $download['file']['id'],
			'url'		=> 'https://www.pitube.ml/downloads/'.$download['file']['id'].'.mp3',
			'title' 	=> $download['file']['title'],
			'size'		=> $download['file']['size'],
			'thumb'		=> $download['file']['thumb'],
			'added_on'	=> date("d/m/Y", strtotime($download['file']['location']))
		);
		echo json_encode($array);
		}
	}
?>