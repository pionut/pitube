# You can find us on 3Konv.com - http://3konv.com/ 

3Konv is a YouTube to MP3 converter, mobile-ready, lightweight & free.
Requirements:
  - PHP 7.0
  - Apache 2.4
  - cURL
  - youtube-dl
  - ffmpeg

# New Features!

  - Search a song by name
  - New download method ( for slow devices )
  - Multiple file download
  - Playlist download
  - Users can now disable the background image to improve performance
  - Automatically sync files with your Android device!


You can also:
  - Get it for Android
  - Use it on any platform


### Tech

3Konv uses a number of open source projects to work properly:

* [jQuery] - duh
* HTML5 / CSS3
* PHP
* JSON

### Installation
Installing on Debian 9:

```sh
$ apt-get install apache php curl wget ffmpeg youtube-dl
$ cd /var/www/html/
$ git clone https://bitbucket.org/pionut/pitube/
```

Want to contribute? Great! Contact me on p.ionut196@gmail.com

** It's not really working? Well...we are doing the best to move everything on the new server as soon as possible..