<?php
/*!
 * PiTube PHP Class
 * https://www.pitube.ml
 * Version 0.2.7
 */

class pitube
{
	private $mysqli;
	private $user_id;

	function __construct() {
		@session_start();
		$mysqli = new mysqli("localhost", "piuser", "e5y5y6e4a", "zadmin_pitube");
		if ($mysqli->connect_errno)
		{
			echo "<br><h1>Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error . "</h1><br>";
		}
		$this->mysqli = $mysqli;
		if(!isset($_SESSION['user_id'])) {
			$look_for_user = $this->mysqli->query("SELECT id, real_name FROM `users` WHERE ip_address = '".$_SERVER['REMOTE_ADDR']."';")->fetch_object();
			if(count($look_for_user)===1){
				$_SESSION['user_id']	= $look_for_user->id;
				$_SESSION['user_name']	= $look_for_user->real_name;
			} else {
				$generate = rand(0,9999);
				$query = $this->mysqli->query("INSERT INTO `users`(`username`, `real_name`, `ip_address`) VALUES ('".$generate."', 'guest', '".$_SERVER['REMOTE_ADDR']."')");
				$_SESSION['user_id']	= $generate;
				$_SESSION['user_name']	= "user";
			}
		}
		$this->user_id		= $_SESSION['user_id'];
		$this->real_name	= $_SESSION['user_name'];		
	}
	
	function get_user_info($request){
		return $this->$request;
	}
	
	function get_history(){
		return $this->mysqli->query('SELECT * FROM `song_history` WHERE `user_id` = '.$this->user_id.' ORDER by timestamp DESC');
	}
	
	function generate_new_files_list(){
		return $this->mysqli->query('SELECT * FROM `song_history` WHERE `user_id` = '.$this->user_id.' AND on_android = 0 ORDER by timestamp DESC');
	}
	
	function update_downloads_android(){
		$query = $this->mysqli->query('UPDATE `song_history` SET `on_android` = 1 WHERE `user_id` = '.$this->user_id.';');
	}
	
	function count_user_files(){
		$sql = $this->mysqli->query('SELECT Count(user_id) as counter FROM song_history WHERE user_id = "'.$this->user_id.'"')->fetch_array(MYSQLI_ASSOC);
		return $sql['counter'];
	}
	
	function check_if_already_downloaded($youtube_id){
		$sql = $this->mysqli->query('SELECT Count(user_id) as counter FROM `song_history` WHERE `youtube_id` = "'.$youtube_id.'" AND `user_id`= "'.$this->user_id.'"')->fetch_array(MYSQLI_ASSOC);
		return $sql['counter'];
	}
	
	function add_song_to_history($youtube_id, $title, $thumb){
		$sql = $this->mysqli->query('SELECT Count(user_id) as counter FROM `song_history` WHERE `youtube_id` = "'.$youtube_id.'" AND `user_id`= "'.$this->user_id.'"')->fetch_array(MYSQLI_ASSOC);
		if($sql['counter'] == 0) {
			$query = $this->mysqli->query('INSERT INTO `song_history`(`user_id`, `youtube_id`, `title`, `thumb`) VALUES ("'.$this->user_id.'", "'.$youtube_id.'", "'.$title.'", "'.$thumb.'")');
		}
	}
	function update_downloads($video_id){
		$query = $this->mysqli->query('UPDATE `song_history` SET `downloads` = downloads + 1 WHERE `youtube_id` = "'.$video_id.'" AND `user_id` = '.$this->user_id.';');
	}
	function update_status($video_id,$status){
		$query = $this->mysqli->query('UPDATE `song_history` SET `status` = 1 WHERE `youtube_id` = "'.$video_id.'" AND `user_id` = '.$this->user_id.';');
	}
	
    public function get_country($ip = null) {
		if(!isset($_SESSION['client_country_code'])) {
			$curl = curl_init("http://ip-api.com/json/".$ip);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$return = curl_exec($curl);
			curl_close($curl);
			$_SESSION['client_country_code'] = json_decode($return, true)['countryCode'];
		}
		return $_SESSION['client_country_code'];
	}
	
    public function song_info($url = null)
	{
		$youtube = "http://www.youtube.com/oembed?url=". $url ."&format=json";
		$curl = curl_init($youtube);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$return = curl_exec($curl);
		curl_close($curl);
		return json_decode($return, true);
	}
	
    public function get_title($page_url = null)
    {
		$title = $this->song_info($page_url);
		if(isset($title['title'])){
			return $title["title"];
		} else {
			$this->kill("Can't get title from YouTube");
		}	
		
    }
	
	public function get_thumbnail($page_url = null)
    {
		$thumb = $this->song_info($page_url);
		if(isset($thumb['thumbnail_url'])){
			return $thumb["thumbnail_url"];
		} else {
			$this->kill("Can't get thumb from YouTube");
		}
    }
	
    
    public function extract_youtube_id($url = null)
	{
		if(!preg_match("/(youtube.com|youtu.be|ytscreeningroom)/i", $url)){
		 $this->kill("Invalid URL");
		}
		$regex = '~(?:http|https|)(?::\/\/|)(?:www.|)(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/ytscreeningroom\?v=|\/feeds\/api\/videos\/|\/user\S*[^\w\-\s]|\S*[^\w\-\s]))([\w\-]{11})[a-z0-9;:@#?&%=+\/\$_.-]*~i';
		$id = preg_replace( $regex, '$1', $url );
		return $id;
	}
	
    public function download($id = null, $path = "downloads")
    {
		$downloadPatch = __DIR__ .'/'.$path.'/';
		if (!file_exists($downloadPatch)) {
			mkdir($downloadPatch);
		}
		$video_id = $this->extract_youtube_id($id);
		$location = $downloadPatch.$video_id.".mp3";
		$title = $this->get_title($id);
		$thumb = $this->get_thumbnail($id);
		$check_if_already_done = $this->check_if_already_downloaded($video_id);
		$this->add_song_to_history($video_id, $title, $thumb);
		if(file_exists($location) == False && $check_if_already_done == 0) {
			$process = 'youtube-dl';
			$options = ' --newline --extract-audio --audio-format mp3 --audio-quality 0 --embed-thumbnail --max-filesize 500m --no-playlist --add-metadata --metadata-from-title "(?P<artist>.+?) - (?P<title>.+)" -o "'.$downloadPatch.'%(id)s.%(ext)s" https://www.youtube.com/watch?v=' . $video_id;
			set_time_limit(0);
			$handle = popen($process.$options, "r");
			if (ob_get_level() == 0)
				ob_start();
			while (!feof($handle)) {
				$buffer = fgets($handle);
					// still work in progress :(
					// echo $this->clean_output($buffer); //get pid output
					// echo "."; //get pid output
				ob_flush();
				flush();
			}
			pclose($handle);
			ob_end_flush();
        }
		$size = $this->readable_filesize($location);
		$sinfo = array(
				'file' => array(
					'id' => $video_id,
					'title' => $title,
					'thumb' => $thumb,
					'location' => $location,
					'size' => $size
				)
			);
		$this->update_status($video_id,1);
		return $sinfo;
    }
    
    public function readable_filesize($location = null)
    {
		if(file_exists($location)) {
			$bytes = @filesize($location);
			$i     = floor(log($bytes, 1024));
			// return round($bytes / pow(1024, $i), [0,0,2,2,3][$i]).['B','kB','MB','GB','TB'][$i];
			return 1;
		} else {
			$this->kill("Can't save file on server!");
		}
    }
    
    public function mobile_notify()
	{
		preg_match("/iPhone|Android|iPad|iPod/", $_SERVER['HTTP_USER_AGENT'], $matches);
		$os = current($matches);
		$notify_os = null;
		switch($os){
		   case 'iPhone': $notify_os	= 'Unfortunately it`s only one way to use this website on a iDevice. <a title="" href="">Read our instructions here</a>'; break;
		   case 'Android': $notify_os	= 'Do you know we have an Android Application? <a title="PiTube for Android" href="">Get it now!</a>'; break;
		   case 'iPad': $notify_os		= 'Unfortunately it`s only one way to use this website on a iDevice. <a title="How to put MP3 into an iDevice" href="">Read our instructions here</a>'; break;
		   case 'iPod': $notify_os		=''; break;
		}
		if(!$notify_os == null){
			echo $notify_os;
		}
	}
	
    public function kill($message = '500 Internal Server Error', $title = 'Server Error')
    {
        @header('HTTP/1.1 500 Internal Server Error');
		@http_response_code(500);
        die(json_encode(array(
            "error" => $title,
            "message" => $message
        )));
    }
	
	public function clean_output($string) {
		//trying to extract the percentage from youtube-dl
		if (strpos($string, '[download]') === 0) {
			$string = str_replace("[download]", "", $string);
			$string = strstr($string, '%', true);
			// $string = preg_replace('/\s+/', '', $string);
			
			return $string;
		}
	}

	public function generate_thumb($source) {
		//when someone it's sharing a link on Social Media we want our logo in a corner
		$image = new Imagick();
		$image->readImage($source);
		$watermark = new Imagick();
		$watermark->readImage(getcwd()."/pitemplate/logo.png");
		$watermarkResizeFactor = 2;
		$img_Width = $image->getImageWidth();
		$img_Height = $image->getImageHeight();
		$watermark_Width = $watermark->getImageWidth();
		$watermark_Height = $watermark->getImageHeight();
		$watermark->scaleImage($watermark_Width / $watermarkResizeFactor, $watermark_Height / $watermarkResizeFactor);
		$watermark_Width = $watermark->getImageWidth();
		$watermark_Height = $watermark->getImageHeight();
		$x = 15;
		//$y = ($img_Height - $watermark_Height) - 15;
		$y = 15;
		$image->compositeImage($watermark, Imagick::COMPOSITE_OVER, $x, $y);
		header("Content-Type: image/" . $image->getImageFormat());
		echo $image;
	}
}
?>